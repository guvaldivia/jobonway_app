(function () {
    'use strict';

    angular.module('app.login')
            .factory('loginService', loginService);

    loginService.$inject = ['$http', 'logger', '$cordovaToast', '$ionicLoading', '$cordovaDialogs', '$state', 'login_url', 'send_profile_url', '$cordovaFileTransfer', 'send_profile_photo_url'];
    function loginService($http, logger, $cordovaToast, $ionicLoading, $cordovaDialogs, $state, login_url, send_profile_url, $cordovaFileTransfer, send_profile_photo_url) {

        var service = {
            autenticar: autenticar,
            isAuthenticated: isAuthenticated,
            storeUser: storeUser,
            getEmpleado: getEmpleado,
            logout: logout,
            sendProfile: sendProfile,
            sendImageProfile: sendImageProfile
        };

        return service;

        function sendImageProfile(empleado_id, foto) {                        
            /*var options = new FileUploadOptions();
            options.httpMethod = "POST";
            options.fileKey = "file";
            options.fileName = foto.substr(foto.lastIndexOf('/') + 1);
            options.mimeType = "image/jpeg";
            options.chunkedMode = true;
            
             var params  = {
                empleado_id: empleado_id
            };            
            options.params = params;
             */
            
            var options  = {
                httpMethod: "POST",
                fileKey: "foto",
                fileName : foto.substr(foto.lastIndexOf('/') + 1),
                mimeType: "image/jpeg",
                chunkedMode: true,
                params: {
                    empleado_id: empleado_id
                }
            };
           

            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner> <p>Loading, please wait...</p>'
            });

            $cordovaFileTransfer.upload(send_profile_photo_url, foto, options)
                    .then(function (result) {                 
                                                
                        $ionicLoading.hide();
                                                
                        var response = JSON.parse(result.response);
                
                        if (response.success) {
                            $cordovaToast.showLongBottom('Success !!! ' + response.message);
                            service.imageName = response.imageName;
                            //Actualizar foto del perfil
                            service.empleado.foto = service.imageName;
                            window.localStorage.setItem('foto', service.empleado.foto);                            

                        } else {
                            $cordovaToast.showLongBottom('Error !!! ' + response.error);
                            logger.error('Error !!! ' + response.error);
                        }
                    }, function (error) {
                        $ionicLoading.hide();
                        $cordovaToast.showLongBottom('Error !!! ' + JSON.stringify(error));
                        logger.error('Error !!' + JSON.stringify(error));
                    }, function (progress) {
                        // constant progress updates
                    });
        }

        function sendProfile(empleado) {
            var settings = {
            };

            service.empleado = empleado;
            var data = {
                empleado_id: empleado.empleado_id,
                nombre: empleado.nombre,
                apellidos: empleado.apellidos,
                email: empleado.email,
                telefono: empleado.telefono,
                direccion: empleado.direccion
            };
       

            return $http.post(send_profile_url, data)
                    .then(success)
                    .catch(failed);

            function success(data, status, headers, config) {                
                if (data.data.success) {
                    $cordovaToast.showLongBottom('Success !!! ' + data.data.message);
                } else {
                    $cordovaToast.showLongBottom('Error !!! ' + data.data.error);
                    logger.error('Error !!! ' + data.data.error);
                }
            }

            function failed(error) {
                $cordovaToast.showLongBottom('Error !!! ' + error.data);
                logger.error('Error !!' + error.data);
            }
        }

        function logout() {
            $cordovaDialogs.confirm('¿Are you sure you want logout?', 'Logout', ['OK', 'Cancel'])
                    .then(function (buttonIndex) {
                        // no button = 0, 'OK' = 1, 'Cancel' = 2
                        var btnIndex = buttonIndex;
                        if (btnIndex == 1) {
                            //Eiminar login
                            window.localStorage.removeItem('login');
                            //Eliminar datos del perfil
                            deleteUser();
                            //Ir al login
                            $state.go('login');
                        } else {
                            //Ir a notificaciones
                            $state.go('tab.settings.notifications');
                        }
                    });

            /*var confirmPopup = $ionicPopup.confirm({
             title: 'Logout',
             template: '¿Are you sure you want logout?',
             cancelText: 'Cancel',
             cancelType: 'button-assertive'
             });
             confirmPopup.then(function (res) {
             if (res) {
             //Eiminar login
             window.localStorage.removeItem('login');
             //Eliminar datos del perfil
             deleteUser();
             
             $state.go('login');
             } else {
             $state.go('tab.settings.notifications');
             }
             });*/
        }

        function isAuthenticated() {
            var login = window.localStorage.getItem('login');
            return login;
        }

        function autenticar(telefono, regid, plataforma) {
            var settings = {
            };

            var data = {
                telefono: telefono,
                regid: regid,
                plataforma: plataforma
            };

            return $http.post(login_url, data)
                    .then(success)
                    .catch(failed);

            function success(data, status, headers, config) {               
                if (data.data.success) {
                    service.empleado = data.data.empleado;

                    storeUser(service.empleado);
                } else {
                    service.empleado = false;
                    $cordovaToast.showLongBottom('Error !!! ' + data.data.error);
                    logger.error('Error !!! ' + data.data.error);
                }
                return service.empleado;
            }

            function failed(error) {               
                $cordovaToast.showLongBottom('Error !!! ' + error.data);
                logger.error('Error !!! ' + error.data);
            }
        }

        function storeUser(empleado) {
            window.localStorage.setItem('empleado_id', empleado.empleado_id);
            window.localStorage.setItem('nombre', empleado.nombre);
            window.localStorage.setItem('apellidos', empleado.apellidos);
            window.localStorage.setItem('email', empleado.email);
            window.localStorage.setItem('foto', empleado.foto);
            window.localStorage.setItem('telefono', empleado.telefono);
            window.localStorage.setItem('celular', empleado.celular);
            window.localStorage.setItem('direccion', empleado.direccion);
        }

        function deleteUser() {
            window.localStorage.removeItem('empleado_id');
            window.localStorage.removeItem('nombre');
            window.localStorage.removeItem('apellidos');
            window.localStorage.removeItem('email');
            window.localStorage.removeItem('foto');
            window.localStorage.removeItem('telefono');
            window.localStorage.removeItem('celular');
            window.localStorage.removeItem('direccion');
        }

        function getEmpleado() {
            service.empleado = {
                empleado_id: window.localStorage.getItem('empleado_id'),
                nombre: window.localStorage.getItem('nombre'),
                apellidos: window.localStorage.getItem('apellidos'),
                email: window.localStorage.getItem('email'),
                foto: window.localStorage.getItem('foto'),
                telefono: window.localStorage.getItem('telefono'),
                celular: window.localStorage.getItem('celular'),
                direccion: window.localStorage.getItem('direccion')
            };

            return service.empleado;
        }
    }

})();

