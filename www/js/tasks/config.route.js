(function () {
    'use strict';

    angular.module('app.tasks')
            .config(tasksConfig);

    tasksConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
    function tasksConfig($stateProvider, $urlRouterProvider) {
        // Redirect any unmatched url
        $urlRouterProvider.otherwise("/tab/tasks");
        
        $stateProvider.state('tab', {
            url: '/tab',
            abstract: true,
            templateUrl: 'js/tasks/tabs.html'
        })
                .state('tab.tasks', {
                    url: '/tasks',
                    views: {
                        'tab-tasks': {
                            templateUrl: 'js/tasks/tasks.html',
                            controller: 'Tasks as vm'
                        }
                    }
                })
                .state('tab.task-detail', {
                    url: '/task/:taskId',
                    views: {
                        'tab-tasks': {
                            templateUrl: 'js/tasks/task-detail.html',
                            controller: 'TaskDetail as vm'
                        }
                    }
                })
                .state('tab.task-map', {
                    url: '/task-map/:taskId',
                    views: {
                        'tab-tasks': {
                            templateUrl: 'js/tasks/task-map.html',
                            controller: 'TaskMap as vm'
                        }
                    }
                });
    }

})();
