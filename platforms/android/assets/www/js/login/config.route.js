(function () {
    'use strict';

    angular.module('app.login')
            .config(loginConfig);

    loginConfig.$inject = ['$stateProvider'];
    function loginConfig($stateProvider) {

        $stateProvider.state('login', {
            url: "/login",
            templateUrl: "js/login/login.html",
            controller: "Login as vm"

        })
        .state('logout', {
            url: '/logout',
            templateUrl: "js/login/login.html"
        });
    }

})();
