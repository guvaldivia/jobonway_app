(function () {
    'use strict';

    angular.module('app.core', [
        "ionic",
        'ngCordova',
        'blocks.exception', 'blocks.logger'

    ])
            .run(run)
            .run(runState)
            .run(runLoading);

    run.$inject = ["$ionicPlatform", '$cordovaToast', '$cordovaLocalNotification', '$cordovaMedia', '$cordovaDevice', 'logger'];
    function run($ionicPlatform, $cordovaToast, $cordovaLocalNotification, $cordovaMedia, $cordovaDevice, logger) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs).
            // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
            // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
            // useful especially with forms, though we would prefer giving the user a little more room
            // to interact with the app.
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // Set the statusbar to use the default style, tweak this to
                // remove the status bar on iOS or change it to use white instead of dark colors.
                StatusBar.styleDefault();
            }

            var push = PushNotification.init({
                android: {
                    senderID: "803511896246"
                },
                ios: {
                    alert: "true",
                    badge: "true",
                    sound: "true"
                },
                windows: {}
            });
            push.on('registration', function (data) {
                var regid = data.registrationId;
                window.localStorage.setItem('regid', regid);
            });
            push.on('notification', function (data) {
                // data.message,
                // data.title,
                // data.count,
                // data.sound,
                // data.image,
                // data.additionalData
                $cordovaLocalNotification.schedule({
                    id: data.id,
                    title: data.title,
                    text: data.message,
                    data: {
                    }
                });
                //Sonar alarma
                var src = "../../sounds/beep.wav";
                var media = $cordovaMedia.newMedia(src);

                var plataforma = $cordovaDevice.getPlatform();
                if (plataforma == 'iOS') {
                    var iOSPlayOptions = {
                        numberOfLoops: 1,
                        playAudioWhenScreenIsLocked: true
                    };
                    media.play(iOSPlayOptions); // iOS only!
                }
                media.play(); // Android
            });
            push.on('error', function (e) {
                // e.message
                $cordovaToast.showLongBottom('Error !!! ' + e.message);
                logger.error('Error !!! ' + e.message);
            });

        });
    }

    runState.$inject = ['$state', '$rootScope', 'loginService'];
    function runState($state, $rootScope, loginService) {

        //window.localStorage.setItem('login', true);
        //window.localStorage.removeItem('login');
        $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
            if (!loginService.isAuthenticated()) {
                if (next.name !== "login") {
                    event.preventDefault();
                    $state.go('login');
                }
            } else {
                if (next.name == "logout") {
                    loginService.logout();
                }
            }
        });

    }

    runLoading.$inject = ['$rootScope', '$ionicLoading'];
    function runLoading($rootScope, $ionicLoading) {
        $rootScope.$on('loading:show', function () {
            var template = '<ion-spinner></ion-spinner> <p>Loading, please wait...</p>';
            $ionicLoading.show({template: template});            
        });

        $rootScope.$on('loading:hide', function () {
            $ionicLoading.hide()
        });
    }

})();
