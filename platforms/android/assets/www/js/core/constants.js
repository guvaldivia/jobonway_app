/* global toastr:false, moment:false  10.0.2.2*/
(function () {
    'use strict';

    angular
            .module('app.core')
            .constant('img_path', 'http://10.0.2.2/jobonway/web/uploads/images/')
            .constant('atachment_path', 'http://10.0.2.2/jobonway/web/uploads/archivos/')
            .constant('login_url', 'http://10.0.2.2/jobonway/web/api/autenticar')
            .constant('list_tasks_url', 'http://10.0.2.2/jobonway/web/api/list-tasks')
            .constant('list_all_tasks_url', 'http://10.0.2.2/jobonway/web/api/list-all-tasks')
            .constant('list_task_completed_url', 'http://10.0.2.2/jobonway/web/api/task-completed')
            .constant('list_task_comment_url', 'http://10.0.2.2/jobonway/web/api/task-comment')
            .constant('list_notifications_url', 'http://10.0.2.2/jobonway/web/api/list-notifications')
            .constant('list_notification_delete', 'http://10.0.2.2/jobonway/web/api/delete-notification')
            .constant('send_profile_url', 'http://10.0.2.2/jobonway/web/api/update-profile')
            .constant('send_profile_photo_url', 'http://10.0.2.2/jobonway/web/api/update-profile-photo')
            ;
})();
