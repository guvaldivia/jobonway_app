/* global toastr:false, moment:false  10.0.2.2*/
(function () {
    'use strict';

    angular
            .module('app.core')
            .constant('img_path', 'https://panel.jobonway.net/uploads/images/')
            .constant('atachment_path', 'https://panel.jobonway.net/uploads/archivos/')
            .constant('login_url', 'https://panel.jobonway.net/api/autenticar')
            .constant('list_tasks_url', 'https://panel.jobonway.net/api/list-tasks')
            .constant('list_all_tasks_url', 'https://panel.jobonway.net/api/list-all-tasks')
            .constant('list_task_completed_url', 'https://panel.jobonway.net/api/task-completed')
            .constant('list_task_comment_url', 'https://panel.jobonway.net/api/task-comment')
            .constant('list_notifications_url', 'https://panel.jobonway.net/api/list-notifications')
            .constant('list_notification_delete', 'https://panel.jobonway.net/api/delete-notification')
            .constant('send_profile_url', 'https://panel.jobonway.net/api/update-profile')
            .constant('send_profile_photo_url', 'https://panel.jobonway.net/api/update-profile-photo')
            ;
})();
