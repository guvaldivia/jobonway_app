(function () {
    'use strict';

    angular.module('app.settings')
            .factory('imageService', imageService);

    imageService.$inject = ['$cordovaCamera', '$q', '$cordovaFile'];
    function imageService($cordovaCamera, $q, $cordovaFile) {


        var service = {
            handMediaDialog: saveMedia,            
        };

        return service;

        function saveMedia(type) {
            return $q(function (resolve, reject) {
                var options = optionsForType(type);
                $cordovaCamera.getPicture(options).then(function (imageUrl) {
                    var name = imageUrl.substr(imageUrl.lastIndexOf('/') + 1);
                    var namePath = imageUrl.substr(0, imageUrl.lastIndexOf('/') + 1);
                    var newName = makeid() + name;

                    $cordovaFile.copyFile(namePath, name, cordova.file.dataDirectory, newName).then(function (info) {
                        
                        service.imageName = newName;                        

                        resolve();
                    }, function (e) {
                        reject();
                    });
                });
            });
        }
        ;

        function makeid() {
            var text = '';
            var posible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 5; i++) {
                text = posible.charAt(Math.floor(Math.random() * posible.length));
            }
            return text;
        }
        ;

        function optionsForType(type) {
            var source;
            switch (type) {
                case 1:
                    source = Camera.PictureSourceType.CAMERA;
                    break;
                case 2:
                    source = Camera.PictureSourceType.PHOTOLIBRARY;
                    break;
            }
            return {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: source,
                allowEdit: false,
                ecodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };
        }
        ;

    }

})();

