(function () {
  'use strict';

  angular.module('app.tasks')
    .factory('taskService', taskService);

  taskService.$inject = ['$http', 'logger', '$cordovaToast', '$ionicLoading', 'list_tasks_url', 'list_all_tasks_url', 'list_task_completed_url', 'list_task_comment_url', '$timeout', '$cordovaFileTransfer', 'atachment_path', '$cordovaDevice', '$cordovaFileOpener2'];
  function taskService($http, logger, $cordovaToast, $ionicLoading, list_tasks_url, list_all_tasks_url, list_task_completed_url, list_task_comment_url, $timeout, $cordovaFileTransfer, atachment_path, $cordovaDevice, $cordovaFileOpener2) {

    var empleado_id = window.localStorage.getItem('empleado_id');
    var tasks = [];

    var service = {
      getTasks: getTasks,
      getAllTasks: getAllTasks,
      completed: completed,
      sendComment: sendComment,
      downloadAtachment: downloadAtachment,
      getTask: getTask
    };

    return service;

    function getTasks(start) {
      var settings = {};

      var data = {
        empleado_id: empleado_id,
        start: start
      };

      return $http.post(list_tasks_url, data)
        .then(success)
        .catch(failed);

      function success(data, status, headers, config) {
        service.tasks = data.data.tasks;
        service.moreDataCanBeLoaded = data.data.moreDataCanBeLoaded;
        tasks = service.tasks;

        return service.tasks;
      }

      function failed(error) {
        logger.error('Error !!' + error.data);
      }
    }

    function getAllTasks() {
      var settings = {};

      var data = {
        empleado_id: empleado_id
      };

      return $http.post(list_all_tasks_url, data)
        .then(success)
        .catch(failed);

      function success(data, status, headers, config) {
        service.alltasks = data.data.tasks;
        tasks = service.alltasks;

        return service.alltasks;
      }

      function failed(error) {
        logger.error('Error !!' + error.data);
      }
    }

    function completed(tarea_id) {
      var settings = {};

      var data = {
        empleado_id: empleado_id,
        tarea_id: tarea_id
      };

      return $http.post(list_task_completed_url, data)
        .then(success)
        .catch(failed);

      function success(data, status, headers, config) {
        if (data.data.success) {
          $cordovaToast.showLongBottom('Success !!! ' + data.data.message);
        } else {
          $cordovaToast.showLongBottom('Error !!! ' + data.data.error);
          logger.error('Error !!! ' + data.data.error);
        }
      }

      function failed(error) {
        $cordovaToast.showLongBottom('Error !!! ' + error.data);
        logger.error('Error !!' + error.data);
      }
    }

    function sendComment(task) {
      var settings = {};

      var data = {
        empleado_id: empleado_id,
        tarea_id: task.tarea_id,
        comentario: task.comentario
      };


      return $http.post(list_task_comment_url, data)
        .then(success)
        .catch(failed);

      function success(data, status, headers, config) {

        if (data.data.success) {
          $cordovaToast.showLongBottom('Success !!! ' + data.data.message);
        } else {
          $cordovaToast.showLongBottom('Error !!! ' + data.data.error);
          logger.error('Error !!! ' + data.data.error);
        }
      }

      function failed(error) {
        $cordovaToast.showLongBottom('Error !!! ' + error.data);
        logger.error('Error !!' + error.data);
      }
    }

    function getTask(id) {
      for (var i = 0; i < tasks.length; i++) {
        if (tasks[i].tarea_id === parseInt(id)) {
          return tasks[i];
        }
      }
      return null;
    }

    function downloadAtachment(tarea) {

      var dataDirectory = cordova.file.externalDataDirectory;
      if (dataDirectory == null) {
        dataDirectory = cordova.file.dataDirectory;
      }

      var plataforma = $cordovaDevice.getPlatform();
      if (plataforma == 'iOS') {
        dataDirectory = cordova.file.documentsDirectory;
      }

      var filePath = dataDirectory + tarea.archivo;
      var url = atachment_path + tarea.archivo;
      var options = {};

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner> <p>Downloading, please wait...</p>'
      });

      $cordovaFileTransfer.download(url, filePath, options, true)
        .then(
          function (result) {
          }, function (error) {
          }, function (progress) {
            $timeout(function () {
              service.downloadProgress = progress.loaded / progress.total;
              if (service.downloadProgress == 1) {
                $ionicLoading.hide();
                $cordovaToast.showLongBottom('Success !!! The operation was successful');
                openFile(filePath, tarea.mime);
              }
            });
          }
        );

    }

    function openFile(filePath, mime) {
      $cordovaFileOpener2.open(filePath, mime).then(function () {
        // file opened successfully
      }, function (err) {
        $cordovaToast.showLongBottom('Error !!! Verify that you have installed an application to view documents');
        // An error occurred. Show a message to the user
      });
    }
  }

})();

