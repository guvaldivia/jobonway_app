(function () {
  'use strict';

  angular
    .module('app.settings')
    .controller('Profile', Profile);

  Profile.$inject = ['$rootScope', '$scope', 'loginService', 'img_path', '$ionicModal', '$cordovaActionSheet', 'imageService'];
  function Profile($rootScope, $scope, loginService, img_path, $ionicModal, $cordovaActionSheet, imageService) {
    var vm = this;
    vm.img_path = img_path;
    //vm.empleado = loginService.getEmpleado();
    //vm.foto = vm.img_path + vm.empleado.foto;

    vm.showDialog = showDialog;
    vm.closeDialog = closeDialog;
    vm.sendProfile = sendProfile;

    vm.editImage = editImage;

    $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
      if (next.name == "tab.settings.profile") {
        activate();
      }
    });

    activate();

    function activate() {
      vm.empleado = loginService.getEmpleado();
      vm.foto = vm.img_path + vm.empleado.foto;
    }

    function editImage() {
      var options = {
        title: 'Edit Photo',
        buttonLabels: ['Take photo', 'Photo from library'],//index starting for 1
        addCancelButtonWithLabel: 'Cancel',
        androidEnableCancelButton: true,
        winphoneEnableCancelButton: true,
        //addDestructiveButtonWithLabel: 'Delete it'
      };

      $cordovaActionSheet.show(options)
        .then(function (btnIndex) {
          if (btnIndex == 1 || btnIndex == 2) {
            addImage(btnIndex);
          } else {
            return;
          }
        });

      /*vm.hideSheet = $ionicActionSheet.show({
       buttons: [
       {text: 'Take photo'},
       {text: 'Photo from library'}
       ],
       titleText: 'Edit Photo',
       cancelText: 'Cancel',
       buttonClicked: function (index) {
       addImage(index);
       }
       });*/
    }

    function addImage(type) {
      $cordovaActionSheet.hide();
      imageService.handMediaDialog(type).then(function () {
        vm.foto = cordova.file.dataDirectory + imageService.imageName;

        //Enviar foto al server
        loginService.sendImageProfile(vm.empleado.empleado_id, vm.foto).then(function () {
          vm.empleado.foto = loginService.imageName;
          vm.foto = vm.img_path + vm.empleado.foto;
        });

        $scope.apply();
      });
    };

    function sendProfile(empleado) {
      vm.empleado = empleado;
      return loginService.sendProfile(vm.empleado).then(function () {
        closeDialog();
      });
    }

    function showDialog() {
      vm.modalProfile.show();
    }

    function closeDialog() {
      vm.modalProfile.hide();
    }

    // Create and load the task Modal
    $ionicModal.fromTemplateUrl('js/settings/edit-profile.html', {
      scope: $scope,
      focusFirstInput: false,
      animation: 'slide-in-up'
    }).then(function (modal) {
      vm.modalProfile = modal;
    });

  }
})();
