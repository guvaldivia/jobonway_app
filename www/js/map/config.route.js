(function () {
    'use strict';

    angular.module('app.map')
            .config(tasksConfig);

    tasksConfig.$inject = ['$stateProvider'];
    function tasksConfig($stateProvider) {
       
        
        $stateProvider
                .state('tab.map', {
                    url: '/map',
                    views: {
                        'tab-map': {
                            templateUrl: 'js/map/map.html',
                            controller: 'Map as vm'
                        }
                    }
                })
                ;
    }

})();
