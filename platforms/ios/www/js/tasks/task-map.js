(function () {
    'use strict';

    angular
            .module('app.tasks')
            .controller('TaskMap', TaskMap);

    TaskMap.$inject = ['$scope', '$stateParams', '$ionicLoading', 'taskService', 'logger', '$cordovaToast'];
    function TaskMap($scope, $stateParams, $ionicLoading, taskService, logger, $cordovaToast) {
        var vm = this;
        var id = $stateParams.taskId;

        vm.task = taskService.getTask(id);

        vm.centerOnMe = centerOnMe;

        activate();

        function activate() {

            $ionicLoading.show({
                template: 'Getting current location...',
                showBackdrop: false
            });

            navigator.geolocation.getCurrentPosition(function (pos) {
                $ionicLoading.hide();

                vm.latitud = pos.coords.latitude;
                vm.longitud = pos.coords.longitude;

                var myLatlng = new google.maps.LatLng(vm.latitud, vm.longitud);

                var mapOptions = {
                    center: myLatlng,
                    zoom: 16,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                vm.map = new google.maps.Map(document.getElementById("map"), mapOptions);

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: vm.map,
                    title: "My Location"
                });

                google.maps.event.addListener(marker, 'click', function () {
                    openInfoWindow(marker);
                });

                vm.marker = marker;

                mostrarMarcadorTarea();

            }, function (error) {
                $ionicLoading.hide();

                vm.latitud = "";
                vm.longitud = "";

                logger.error('Unable to get location: ' + error.message);
                $cordovaToast.showLongBottom('Unable to get location: ' + error.message);
            });
        }

        function mostrarMarcadorTarea() {
            //Eliminar marcadores anteriores
            eliminarMarcadores();

            var direccion = vm.task.direccion;
            var latitud = vm.task.latitud;
            var longitud = vm.task.longitud;

            if (direccion != "") {

                var marker = new google.maps.Marker({
                    map: vm.map, //el mapa creado en el paso anterior
                    position: new google.maps.LatLng(latitud, longitud), //objeto con latitud y longitud
                    draggable: false,
                    title: vm.task.nombre //que el marcador se pueda arrastrar
                });

                google.maps.event.addListener(marker, 'click', function () {
                    openInfoWindow(marker);
                });

                vm.markerTarea = marker;
            }
        }

        function openInfoWindow(marker) {
            var markerLatLng = marker.getPosition();
            var infoWindow = new google.maps.InfoWindow();

            infoWindow.setContent('<b>' + marker.getTitle() + '</b><br/>' + markerLatLng.lat() + ", " + markerLatLng.lng());
            infoWindow.open(vm.map, marker);
        }

        function eliminarMarcadores() {
            if (!vm.map) {
                return;
            }
            if (vm.markerTarea) {
                vm.markerTarea.setMap(null);
            }
        }

        function centerOnMe() {
            if (!vm.map) {
                return;
            }
            $ionicLoading.show({
                template: 'Getting current location...',
                showBackdrop: false
            });

            navigator.geolocation.getCurrentPosition(function (pos) {
                $ionicLoading.hide();

                vm.latitud = pos.coords.latitude;
                vm.longitud = pos.coords.longitude;

                var myLatlng = new google.maps.LatLng(vm.latitud, vm.longitud);
                vm.map.setCenter(myLatlng);
                vm.map.setZoom(16);

                //Elimina el marcador previo                
                vm.marker.setMap(null);

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: vm.map,
                    title: "My Location"
                });

                google.maps.event.addListener(marker, 'click', function () {
                    openInfoWindow(marker);
                });

                vm.marker = marker;


            }, function (error) {
                $ionicLoading.hide();

                vm.latitud = "";
                vm.longitud = "";

                logger.error('Unable to get location: ' + error.message);
                $cordovaToast.showLongBottom('Unable to get location: ' + error.message);
            });

        }
    }
})();
