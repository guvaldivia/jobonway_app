(function () {
    'use strict';

    angular.module('app.settings')
            .config(settingsConfig);

    settingsConfig.$inject = ['$stateProvider'];
    function settingsConfig($stateProvider) {


        $stateProvider
                .state('tab.settings', {
                    url: '/settings',
                    abstract: true,
                    views: {
                        'tab-settings': {
                            templateUrl: 'js/settings/menu.html'
                        }
                    }                    
                })
                .state('tab.settings.notifications', {
                    url: '/notifications',
                    views: {
                        'side-menu': {
                            templateUrl: 'js/settings/notifications.html',
                            controller: 'Notifications as vm'
                        }
                    }
                })
                .state('tab.settings.profile', {
                    url: '/profile',
                    views: {
                        'side-menu': {
                            templateUrl: 'js/settings/profile.html',
                            controller: 'Profile as vm'
                        }
                    }
                })
                ;
    }

})();
