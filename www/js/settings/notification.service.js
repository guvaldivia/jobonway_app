(function () {
    'use strict';

    angular.module('app.settings')
            .factory('notificationService', notificationService);

    notificationService.$inject = ['$http', 'logger', '$cordovaToast', 'list_notifications_url', 'list_notification_delete'];
    function notificationService($http, logger, $cordovaToast, list_notifications_url, list_notification_delete) {

        var empleado_id = window.localStorage.getItem('empleado_id');
        var notificaciones = [];
        
        var service = {
            getNotificaciones: getNotificaciones,
            deleteNotification: deleteNotification,
        };

        return service;             
        
        function getNotificaciones(start) {
            var settings = {
            };

            var data = {
                empleado_id: empleado_id,
                start: start
            };

            return $http.post(list_notifications_url, data)
                    .then(success)
                    .catch(failed);

            function success(data, status, headers, config) {
                service.notificaciones = data.data.notificaciones;
                service.moreDataCanBeLoaded = data.data.moreDataCanBeLoaded;
                notificaciones = service.notificaciones;
                
                return service.notificaciones;
            }

            function failed(error) {
                logger.error('Error !!' + error.data);
            }
        }
        
        function deleteNotification(notificacion_id) {
            var settings = {
            };

            var data = {
                notificacion_id: notificacion_id
            };

            return $http.post(list_notification_delete, data)
                    .then(success)
                    .catch(failed);

            function success(data, status, headers, config) {                
                if (data.data.success) {
                    $cordovaToast.showLongBottom('Success !!! ' + data.data.message);
                    
                    return true;
                } else {
                    $cordovaToast.showLongBottom('Error !!! ' + data.data.error);
                    logger.error('Error !!! ' + data.data.error);
                    
                    return false;
                }
                
            }

            function failed(error) {                
                $cordovaToast.showLongBottom('Error !!! ' + error.data);
                logger.error('Error !!' + error.data);
                
                return false;
            }
        }
      
    }

})();

