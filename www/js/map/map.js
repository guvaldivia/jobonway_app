(function () {
    'use strict';

    angular
            .module('app.map')
            .controller('Map', Map);

    Map.$inject = ['$scope', '$ionicLoading', 'taskService', 'logger', '$cordovaToast'];
    function Map($scope, $ionicLoading, taskService, logger, $cordovaToast) {
        var vm = this;
        vm.tasks = [];
        vm.markers = [];

        vm.getTasks = getTasks;
        vm.centerOnMe = centerOnMe;

        activate();

        function activate() {
            $ionicLoading.show({
                template: 'Getting current location...',
                showBackdrop: false
            });

            navigator.geolocation.getCurrentPosition(function (pos) {
                $ionicLoading.hide();

                vm.latitud = pos.coords.latitude;
                vm.longitud = pos.coords.longitude;

                var myLatlng = new google.maps.LatLng(vm.latitud, vm.longitud);

                var mapOptions = {
                    center: myLatlng,
                    zoom: 16,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                vm.map = new google.maps.Map(document.getElementById("map"), mapOptions);             

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: vm.map,
                    title: "My Location"
                });

                google.maps.event.addListener(marker, 'click', function () {
                    openInfoWindow(marker);
                });

                vm.marker = marker;

                //Listar tasks
                $ionicLoading.show({
                    template: '<ion-spinner></ion-spinner> <p>Loading, please wait...</p>'
                });
                return getTasks().then(function () {
                    $ionicLoading.hide();
                    //mostrar marcadores de tarea
                    mostrarMarcadoresTareas();
                });

            }, function (error) {
                $ionicLoading.hide();

                vm.latitud = "";
                vm.longitud = "";

                logger.error('Unable to get location: ' + error.message);
                $cordovaToast.showLongBottom('Unable to get location: ' + error.message);
            });
        }


        function mostrarMarcadoresTareas() {
            //Eliminar marcadores anteriores
            eliminarMarcadores();

            for (var i = 0; i < vm.tasks.length; i++) {
                var direccion = vm.tasks[i].direccion;
                var latitud = vm.tasks[i].latitud;
                var longitud = vm.tasks[i].longitud;

                if (direccion != "") {

                    var marker = new google.maps.Marker({
                        map: vm.map, //el mapa creado en el paso anterior
                        position: new google.maps.LatLng(latitud, longitud), //objeto con latitud y longitud
                        draggable: false,
                        title: vm.tasks[i].nombre //que el marcador se pueda arrastrar
                    });

                    google.maps.event.addListener(marker, 'click', function () {
                        openInfoWindow(marker);
                    });

                    vm.markers.push(marker);
                }
            }
        }

        function openInfoWindow(marker) {
            var markerLatLng = marker.getPosition();
            var infoWindow = new google.maps.InfoWindow();

            infoWindow.setContent('<b>' + marker.getTitle() + '</b><br/>' + markerLatLng.lat() + ", " + markerLatLng.lng());
            infoWindow.open(vm.map, marker);
        }

        function eliminarMarcadores() {
            if (!vm.map) {
                return;
            }
            for (var i = 0; i < vm.markers.length; i++) {
                vm.markers[i].setMap(null);
            }
            vm.markers = new Array();
        }

        function centerOnMe() {
            if (!vm.map) {
                return;
            }
            $ionicLoading.show({
                template: 'Getting current location...',
                showBackdrop: false
            });

            navigator.geolocation.getCurrentPosition(function (pos) {
                $ionicLoading.hide();

                vm.latitud = pos.coords.latitude;
                vm.longitud = pos.coords.longitude;

                var myLatlng = new google.maps.LatLng(vm.latitud, vm.longitud);
                vm.map.setCenter(myLatlng);
                vm.map.setZoom(16);
                
                //Elimina el marcador previo                
                vm.marker.setMap(null);
                
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: vm.map,
                    title: "My Location"
                });

                google.maps.event.addListener(marker, 'click', function () {
                    openInfoWindow(marker);
                });

                vm.marker = marker;


            }, function (error) {
                $ionicLoading.hide();

                vm.latitud = "";
                vm.longitud = "";

                logger.error('Unable to get location: ' + error.message);
                $cordovaToast.showLongBottom('Unable to get location: ' + error.message);
            });

        }

        function getTasks() {
            return taskService.getAllTasks().then(function (data) {
                vm.tasks = data;
                return vm.tasks;
            });
        }

    }
})();
