(function () {
  'use strict';

  angular
    .module('app.tasks')
    .controller('TaskDetail', TaskDetail);

  TaskDetail.$inject = ['$scope', '$stateParams', 'taskService', '$ionicModal', '$cordovaDialogs', 'img_path'];
  function TaskDetail($scope, $stateParams, taskService, $ionicModal, $cordovaDialogs, img_path) {
    var vm = this;
    vm.img_path = img_path;
    var id = $stateParams.taskId;

    vm.task = taskService.getTask(id);

    vm.completed = completed;

    vm.showDialog = showDialog;
    vm.closeDialog = closeDialog;
    vm.sendComment = sendComment;
    vm.downloadAtachment = downloadAtachment;

    function downloadAtachment() {
      return taskService.downloadAtachment(vm.task).then(function () {

      });
    }

    function completed() {
      $cordovaDialogs.confirm('¿Are you sure you want to end the task?', 'End Task', ['OK', 'Cancel'])
        .then(function (buttonIndex) {
          // no button = 0, 'OK' = 1, 'Cancel' = 2
          var btnIndex = buttonIndex;
          if (btnIndex == 1) {
            taskService.completed(vm.task.tarea_id).then(function (data) {
              vm.task.completed = !vm.task.completed;
              vm.task.estado = 'Finish';
            });
          } else {
            return;
          }
        });

    }

    function sendComment() {
      return taskService.sendComment(vm.task).then(function () {
        closeDialog();
      });
    }

    function showDialog() {
      vm.modalComment.show();
    }

    function closeDialog() {
      vm.modalComment.hide();
    }

    // Create and load the task Modal
    $ionicModal.fromTemplateUrl('js/tasks/add-comment.html', {
      scope: $scope,
      focusFirstInput: true,
      animation: 'slide-in-up'
    }).then(function (modal) {
      vm.modalComment = modal;
    });

    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
      vm.modalComment.remove();
    });

  }
})();
