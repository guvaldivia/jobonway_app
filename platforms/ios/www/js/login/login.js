(function () {
    'use strict';

    angular
            .module('app.login')
            .controller('Login', Login);

    Login.$inject = ['$scope', '$state', '$cordovaDevice', 'loginService'];
    function Login($scope, $state, $cordovaDevice, loginService) {
        var vm = this;

        vm.login = login;

        activate();

        function activate() {
        }

        function login() {
            if (vm.telefono != "") {
                vm.regid = window.localStorage.getItem('regid');
                vm.plataforma = "Android";
                //vm.plataforma = $cordovaDevice.getPlatform();

                loginService.autenticar(vm.telefono, vm.regid, vm.plataforma).then(function (data) {
                    vm.empleado = data;
                    if (vm.empleado) {
                        //Salvar preferencias de login
                        window.localStorage.setItem('login', true);
                        //ir a las tasks
                        $state.go('tab.tasks');
                    }
                });

            }
        }
        ;
    }
})();
