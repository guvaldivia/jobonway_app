(function () {
    'use strict';

    angular
            .module('app.settings')
            .controller('Notifications', Notifications);

    Notifications.$inject = ['$scope', '$ionicListDelegate', '$ionicLoading', 'notificationService', '$cordovaDialogs'];
    function Notifications($scope, $ionicListDelegate, $ionicLoading, notificationService, $cordovaDialogs) {
        var vm = this;

        vm.moreDataCanBeLoaded = true;
        vm.start = 0;

        vm.notificaciones = [];

        vm.shouldShowDelete = false;

        vm.getNotificaciones = getNotificaciones;
        vm.loadMore = loadMore;
        vm.doRefresh = doRefresh;
        vm.deleteNotification = deleteNotification;

        function loadMore() {
            //Listar tasks
            /*$ionicLoading.show({
                template: '<ion-spinner></ion-spinner> <p>Loading, please wait...</p>'
            });*/
            return getNotificaciones().then(function () {
                //$ionicLoading.hide();

            });
        }

        function deleteNotification(index) {
            $cordovaDialogs.confirm('¿Are you sure you want to delete selected notification?', 'Delete', ['OK', 'Cancel'])
                    .then(function (buttonIndex) {
                        // no button = 0, 'OK' = 1, 'Cancel' = 2
                        var btnIndex = buttonIndex;
                        if (btnIndex == 1) {
                            notificationService.deleteNotification(vm.notificaciones[index].notificacion_id).then(function (data) {
                                if (data) {
                                    vm.notificaciones.splice(index, 1);
                                    //$ionicListDelegate.closeOptionButtons();
                                }
                            });
                        } else {
                            return;
                        }
                    });
            /*var confirmPopup = $ionicPopup.confirm({
             title: 'Delete',
             template: '¿Are you sure you want to delete selected notification?',
             cancelText: 'Cancel',
             cancelType: 'button-assertive'
             });
             confirmPopup.then(function (res) {
             if (res) {
             notificationService.deleteNotification(vm.notificaciones[index].notificacion_id).then(function (data) {
             if (data) {
             vm.notificaciones.splice(index, 1);
             //$ionicListDelegate.closeOptionButtons();
             }
             });
             
             } else {
             return;
             }
             });*/

        }

        function doRefresh() {
            //Listar tasks            
            return notificationService.getNotificaciones(0).then(function (data) {
                vm.moreDataCanBeLoaded = notificationService.moreDataCanBeLoaded;
                vm.notificaciones = data;
               
                $scope.$broadcast('scroll.refreshComplete');

            });
        }

        function getNotificaciones() {
            return notificationService.getNotificaciones(vm.start).then(function (data) {
                vm.moreDataCanBeLoaded = notificationService.moreDataCanBeLoaded;
                vm.start += data.length;
                vm.notificaciones = vm.notificaciones.concat(data);
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        }

    }
})();
