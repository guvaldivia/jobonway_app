(function () {
    'use strict';

    angular
            .module('app.tasks')
            .controller('Tasks', Tasks);

    Tasks.$inject = ['$scope', '$ionicListDelegate', '$ionicLoading', 'taskService', '$ionicModal', '$state', '$cordovaDialogs', 'img_path'];
    function Tasks($scope, $ionicListDelegate, $ionicLoading, taskService, $ionicModal, $state, $cordovaDialogs, img_path) {
        var vm = this;
        vm.img_path = img_path;
        vm.tasks = [];

        vm.moreDataCanBeLoaded = true;
        vm.start = 0;


        vm.goDetails = goDetails;

        vm.doRefresh = doRefresh;
        vm.completed = completed;

        vm.getTasks = getTasks;
        vm.loadMore = loadMore;

        vm.showDialogComment = showDialogComment;
        vm.closeDialog = closeDialog;
        vm.sendComment = sendComment;

        vm.downloadAtachment = downloadAtachment;


        function downloadAtachment(task) {
            vm.task = task;
            return taskService.downloadAtachment(vm.task).then(function () {

            });

        }


        function loadMore() {
            //Listar tasks
            /*$ionicLoading.show({
                template: '<ion-spinner></ion-spinner> <p>Loading, please wait...</p>'

            });*/
            return getTasks().then(function () {
                //$ionicLoading.hide();
            });
        }

        function goDetails(tarea_id) {
            $state.go('tab.task-detail', {taskId: tarea_id});
        }

        function sendComment(task) {
            vm.task = task;
            return taskService.sendComment(vm.task).then(function () {
                closeDialog();
            });
        }

        function showDialogComment(task) {
            vm.task = task;
            vm.modalComment.show();
        }

        function closeDialog() {
            vm.modalComment.hide();
            $ionicListDelegate.closeOptionButtons();
        }

        function goDetails(tarea_id) {
            $state.go('tab.task-detail', {taskId: tarea_id});
        }

        function sendComment(task) {
            vm.task = task;
            return taskService.sendComment(vm.task).then(function () {
                closeDialog();
            });
        }

        function showDialog(task) {
            vm.task = task;
        }

        function doRefresh() {
            //Listar tasks
            return taskService.getTasks(0).then(function (data) {
                vm.moreDataCanBeLoaded = taskService.moreDataCanBeLoaded;
                vm.tasks = data;

                $scope.$broadcast('scroll.refreshComplete');

            });
        }

        function completed(task) {
            vm.task = task;

            $cordovaDialogs.confirm('¿Are you sure you want to end the task?', 'End Task', ['OK', 'Cancel'])
            .then(function (buttonIndex) {
                // no button = 0, 'OK' = 1, 'Cancel' = 2
                var btnIndex = buttonIndex;
                if (btnIndex == 1) {
                    taskService.completed(task.tarea_id).then(function (data) {
                        task.completed = !task.completed;
                        task.estado = 'Finish';
                        $ionicListDelegate.closeOptionButtons();
                    });
                } else {
                    return;
                }
            });

        }

        function getTasks() {
            return taskService.getTasks(vm.start).then(function (data) {
                vm.moreDataCanBeLoaded = taskService.moreDataCanBeLoaded;
                vm.start += data.length;
                vm.tasks = vm.tasks.concat(data);
                $scope.$broadcast('scroll.infiniteScrollComplete');

            });
        }

        // Create and load the task Modal
        $ionicModal.fromTemplateUrl('js/tasks/add-comment.html', {
            scope: $scope,
            focusFirstInput: true,
            animation: 'slide-in-up'
        }).then(function (modal) {
            vm.modalComment = modal;
        });

        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            vm.modalComment.remove();
        });
    }
})();
