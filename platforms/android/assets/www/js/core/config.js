(function () {
    'use strict';

    var core = angular.module('app.core');

    core.config(ionicConfig);
    core.config(loadingConfig);

    ionicConfig.$inject = ['$ionicConfigProvider'];
    function ionicConfig($ionicConfigProvider) {

        $ionicConfigProvider.navBar.alignTitle('center');
        $ionicConfigProvider.backButton.previousTitleText(true);
        $ionicConfigProvider.tabs.position('bottom');
    }

    loadingConfig.$inject = ['$httpProvider'];
    function loadingConfig($httpProvider) {

        $httpProvider.interceptors.push(function ($rootScope) {
            return {
                request: function (config) {
                    $rootScope.$broadcast('loading:show');
                    return config;
                },
                response: function (response) {
                    $rootScope.$broadcast('loading:hide');
                    return response;
                }
            }
        });
        
    }

})();
